package com.falabella.facade;

import com.falabella.strategy.Animal;

import java.util.List;

public class ZooAdmin {


    public void adminZoo(List<Animal> animals){

        for (Animal a : animals) {
            a.eat();
            a.sleep();
        }
    }
}
