package com.falabella.strategy;

public class Mammoth extends Animal {
    @Override
    public void eat() {
        System.out.println("El mamut quiere comer, pero esta muerto");
    }

    @Override
    public void sleep() {
        System.out.println("El mamut va a dormir para siempre");
    }

    @Override
    public void putInjection() {
        System.out.println("Al mamut le ponemos la inyeccion en la trompa");
    }
}
