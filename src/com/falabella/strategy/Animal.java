package com.falabella.strategy;

public abstract class Animal implements AnimalActions {
    protected int height;
    protected int width;
    protected int age;
}
