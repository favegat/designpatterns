package com.falabella.strategy;

public class Falcon extends Animal {
    @Override
    public void eat() {
        System.out.println("El halcon para comer: caza raton");
    }

    @Override
    public void sleep() {
        System.out.println("El halcon para dormir: se sube al arbol");
    }

    @Override
    public void putInjection() {
        System.out.println("Al halcon le ponemos la inyeccion: bajo el ala");
    }
}
