package com.falabella.strategy;

public class Zuricate extends Animal {
    @Override
    public void eat() {
        System.out.println("La zuricata: come animales pequeños");
    }

    @Override
    public void sleep() {
        System.out.println("La zuricata al dormir: se mete a la madriguera");
    }

    @Override
    public void putInjection() {
        System.out.println("La zuricata, cuando le van a poner una inyeccion: se arranca");
    }
}
