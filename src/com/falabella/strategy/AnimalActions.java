package com.falabella.strategy;

public interface AnimalActions {
    void eat();
    void sleep();
    void putInjection();
}
