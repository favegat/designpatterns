package com.falabella.strategy;

public class Jiraffe extends Animal {
    @Override
    public void eat() {
        System.out.println("La jirafa: se estira a alcanzar las hojas de la cuspide mas alta del zoologico");
    }

    @Override
    public void sleep() {
        System.out.println("La jirafa duerme haciendo: zzzzz");
    }

    @Override
    public void putInjection() {
        System.out.println("A la jirafa le ponemos la inyeccion: en el cuello");
    }
}
