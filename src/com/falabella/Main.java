package com.falabella;

import com.falabella.decorator.ZooAdminVet;
import com.falabella.facade.ZooAdmin;
import com.falabella.factory.AnimalFactory;
import com.falabella.singleton.Zoo;


public class Main {

    public static void main(String[] args) throws Exception {
        ZooAdmin zooAdmin = new ZooAdmin();
        ZooAdminVet zooAdminVet = new ZooAdminVet(zooAdmin);
        Zoo zoo = Zoo.getZoo();
        zoo.setJails(3);

        zoo.setAnimals(AnimalFactory.getAnimalsFromList());
        System.out.println(zoo.getAnimals());

        zooAdminVet.adminZoo(zoo.getAnimals());
    }
}
