package com.falabella.factory;

import com.falabella.strategy.*;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.List;

public class AnimalFactory {
    public static Animal getAnimalsFromList(String animal) {
        if (animal.equals("jiraffe"))
            return new Jiraffe();
        if (animal.equals("zuricate"))
            return new Zuricate();
        if (animal.equals("mammoth"))
            return new Mammoth();
        else
            return new Falcon();
    }

    public static List<Animal> getAnimalsFromList() throws Exception{
        List<Animal> animals = new ArrayList<>();
        File file = new File("/home/favegat/Falabella/Projects/Code/DesignPatterns/src/animal_list.txt");

        BufferedReader br = new BufferedReader(new FileReader(file));

        String st;
        while ((st = br.readLine()) != null){
            animals.add(getAnimalsFromList(st));
        }

        return animals;
    }
}
