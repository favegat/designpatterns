package com.falabella.decorator;

import com.falabella.facade.ZooAdmin;
import com.falabella.strategy.Animal;

import java.util.List;

public class ZooAdminVet extends ZooAdmin {
    private ZooAdmin zooAdmin;

    @Override
    public void adminZoo(List<Animal> animals) {
        super.adminZoo(animals);

        for (Animal a: animals) {
            this.putInyection(a);
        }
    }

    public ZooAdminVet(ZooAdmin zooAdmin) {
        this.zooAdmin = zooAdmin;
    }

    private void putInyection(Animal a) {
        a.putInjection();
    }
}
