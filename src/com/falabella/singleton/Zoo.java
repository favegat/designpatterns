package com.falabella.singleton;

import com.falabella.strategy.Animal;

import java.util.ArrayList;
import java.util.List;

public class Zoo {
    private static Zoo zoo;

    private Zoo() { }
    private static List<Animal> animals;
    private int jails;

    public static Zoo getZoo() {
        if (zoo == null){
            animals = new ArrayList<>();
            zoo = new Zoo();
        }

        return zoo;
    }

    public int getJails(){
        return this.jails;
    }
    public void setJails(int jails){
        this.jails = jails;
    }

    public List<Animal> getAnimals() {
        return animals;
    }

    public void setAnimals(List<Animal> animals) {
        this.animals = animals;
    }
}
